# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:16.13.1 as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run prod 



# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:alpine

# COPY /nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=node /app/dist/amo.web/ /usr/share/nginx/html
