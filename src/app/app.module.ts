import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppComponent } from "./app.component";
import { NavMenuComponent } from "./components/nav-menu/nav-menu.component";
import { HomeComponent } from "./components/home/home.component";

import { ExpenseComponent } from "./components/expense/expense.component";

import { InvoiceComponent } from "./components/invoice/invoice.component";
import { ExpenseService } from "./services/expense.service";
import { InvoiceService } from "./services/invoice.service";
import { TransportService } from "./services/transport.service";
import { TransportComponent } from "./components/transport/transport.component";
import { TransportUpdateComponent } from "./components/transport/transportupdate.component";
import { AmoHttpInterceptor } from "./http-interceptor";
import { ToastrModule } from "ngx-toastr";
import { ExpenseUpdateComponent } from "./components/expense/expenseupdate.component";
import { DatePipe } from '@angular/common';

import { MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule} from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { VehicleComponent } from "./components/vehicle/vehicle.component";
import { VehicleService } from "./services/vehicle.service";
import { VehicleUpdateComponent } from "./components/vehicle/vehicleupdate.component";
 

  



@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    InvoiceComponent,
    ExpenseComponent,
    ExpenseUpdateComponent,
    VehicleComponent,
    VehicleUpdateComponent,
    TransportComponent,
    TransportUpdateComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "ng-cli-universal" }),
    HttpClientModule,
    FormsModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule,
    ToastrModule.forRoot(),

    RouterModule.forRoot([
      { path: "", component: HomeComponent, pathMatch: "full" },
      { path: "expense", component: ExpenseComponent },
      { path: "expenseedit", component: ExpenseUpdateComponent },
      { path: "invoice", component: InvoiceComponent },
      { path: "transport", component: TransportComponent },
      { path: 'transportupdate', component: TransportUpdateComponent },
      { path: "vehicle", component: VehicleComponent },
      { path: "vehicleedit", component: VehicleUpdateComponent },
    ]),
  ],
  providers: [
    DatePipe,
    ExpenseService,
    TransportService,
    InvoiceService,
    VehicleService,
    { provide: HTTP_INTERCEPTORS, useClass: AmoHttpInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
