import { Component, Inject, Pipe, PipeTransform } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { VehicleService } from "../../services/vehicle.service";
import { Vehicle } from "../../datamodel/Vehicle";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { DatePipe } from "@angular/common";
import { MatDateRangePicker, MatDatepickerToggle ,  MatDateRangeInput } from "@angular/material/datepicker";

@Component({
  selector: "app-vehicleupdate-component",
  templateUrl: "./vehicleupdate.component.html",
})
export class VehicleUpdateComponent {
  vehicleForm: FormGroup;

  constructor(
    private vehicleService: VehicleService,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.vehicleForm = new FormGroup({
      class: new FormControl("", [
        Validators.required,
        Validators.maxLength(15),
      ]),
    
      id: new FormControl(100.0),
      name: new FormControl("1/1/2012", Validators.required),
      createdDate: new FormControl("", [
        Validators.required,
        Validators.maxLength(15),
      ]),
    });
  }

  public vehicle: Vehicle;

  onSubmit() {
    this.vehicle = <Vehicle>this.vehicleForm.value;
    console.log(this.vehicle);

    this.vehicleService.saveVehicle(this.vehicle);
  }
}
