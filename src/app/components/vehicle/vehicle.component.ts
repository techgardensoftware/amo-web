import { Component, Inject, OnInit } from "@angular/core";
import { VehicleService } from "../../services/vehicle.service";
import { Vehicle } from "../../datamodel/Vehicle";
import { ThemePalette } from "@angular/material/core";
import { ProgressSpinnerMode } from "@angular/material/progress-spinner";
import { ToastrService } from 'ngx-toastr';
import { CanActivate, Router } from '@angular/router';

// adding comment to check git workflow -- amal training
@Component({
  selector: "app-vehicle-component",
  templateUrl: "./vehicle.component.html",
})
export class VehicleComponent {
  public vehicles: Vehicle[];
color: ThemePalette = 'accent';
mode: ProgressSpinnerMode = 'indeterminate';
value = 100;



  constructor(private vehicleService: VehicleService, 
              private toastr:ToastrService,
              private router: Router) {}

  ngOnInit() {
    this.vehicleService.getVehicles().subscribe((result) => {
      this.toastr.success('Fetched vehicle data from server');
      this.vehicles = result;
    })
  }

  openEditVehicle()
  {
    console.log('opening edit');
    this.router.navigateByUrl("/vehicleedit");
  }
}