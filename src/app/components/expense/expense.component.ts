import { Component, Inject, OnInit } from "@angular/core";
import { ExpenseService } from "../../services/expense.service";
import { Expense } from "../../datamodel/Expense";
import { ThemePalette } from "@angular/material/core";
import { ProgressSpinnerMode } from "@angular/material/progress-spinner";
import { ToastrService } from 'ngx-toastr';
import { CanActivate, Router } from '@angular/router';

// adding comment to check git workflow -- amal training
@Component({
  selector: "app-expense-component",
  templateUrl: "./expense.component.html",
})
export class ExpenseComponent {
  public expenses: Expense[];
color: ThemePalette = 'accent';
mode: ProgressSpinnerMode = 'indeterminate';
value = 100;



  constructor(private expenseService: ExpenseService, 
              private toastr:ToastrService,
              private router: Router) {}

  ngOnInit() {
    this.expenseService.getExpenses().subscribe((result) => {
      this.toastr.success('Fetched expense data from server');
      this.expenses = result;
    })
  }

  openEditExpense()
  {
    console.log('opening edit');
    this.router.navigateByUrl("/expenseedit");
  }
}
