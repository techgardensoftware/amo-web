import { Component, Inject, Pipe, PipeTransform } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ExpenseService } from "../../services/expense.service";
import { Expense } from "../../datamodel/Expense";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { DatePipe } from "@angular/common";
import { MatDateRangePicker, MatDatepickerToggle ,  MatDateRangeInput } from "@angular/material/datepicker";

@Component({
  selector: "app-expenseupdate-component",
  templateUrl: "./expenseupdate.component.html",
})
export class ExpenseUpdateComponent {
  expenseForm: FormGroup;

  constructor(
    private expenseService: ExpenseService,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.expenseForm = new FormGroup({
      approvedBy: new FormControl("", [
        Validators.required,
        Validators.maxLength(15),
      ]),
      expenseDetails: new FormControl("", [
        Validators.required,
        Validators.maxLength(15),
      ]),
      amount: new FormControl(100.0),
      createdDate: new FormControl("1/1/2012", Validators.required),
      createdBy: new FormControl("", [
        Validators.required,
        Validators.maxLength(15),
      ]),
    });
  }

  public expense: Expense;

  onSubmit() {
    this.expense = <Expense>this.expenseForm.value;
    console.log(this.expense);

    this.expenseService.saveExpense(this.expense);
  }
}
