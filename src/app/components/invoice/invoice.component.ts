import { Component, Inject } from "@angular/core";

import { InvoiceService } from "../../services/invoice.service";
import { Invoice } from "../../datamodel/Invoice";

@Component({
  selector: "app-invoice-component",
  templateUrl: "./invoice.component.html",
})
export class InvoiceComponent {
  public invoices: Invoice[];

  constructor(private invoiceService: InvoiceService) {}

  ngOnInit() {
    this.invoiceService.getInvoices().subscribe((result) => {
      this.invoices = result;
    });
  }
}
