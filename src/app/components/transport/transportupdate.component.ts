
import { Component, Inject } from "@angular/core";
import { TransportService } from "../../services/transport.service";
import { Transport } from "../../datamodel/Transport";
import { Router } from "@angular/router";

@Component({
  selector: "app-transportupdate-component",
  templateUrl: "./transportupdate.component.html",
})
export class TransportUpdateComponent {
  public transport: Transport;

  constructor(private transportService: TransportService, private router:Router) {}

  ngOnInit() {
    }

    saveNew(){
        this.transportService.saveTransport(this.transport).subscribe((data) => console.log(data));
    }
  }