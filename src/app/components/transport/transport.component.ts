import { Component, Inject } from "@angular/core";
import { TransportService } from "../../services/transport.service";
import { Transport } from "../../datamodel/Transport";
import { Router } from "@angular/router";

@Component({
  selector: "app-transport-component",
  templateUrl: "./transport.component.html",
})
export class TransportComponent {
  public transports: Transport[];
  

  constructor(private transportService: TransportService, private router:Router) {}

  ngOnInit() {
    this.transportService.getTransport().subscribe((result) => {
      this.transports = result;
    });
    
  }
 
  openForm() {
      this.router.navigate(['/transportupdate']);
  }
   
 } 
