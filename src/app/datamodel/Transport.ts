export interface Transport {
    modeOfTransport: string;
    appliedDate: string;
    minAmount: number;
    distance: number;
    status: string;
 }