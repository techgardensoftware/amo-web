export interface Vehicle {
    id: number;
    name: string;
    class: string;
    createdDate: string;

  }