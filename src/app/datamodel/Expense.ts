export interface Expense {
  createdBy: string;
  createdDate: string;
  amount: number;
  expenseDetails: string;
  approvedBy: string;
}
