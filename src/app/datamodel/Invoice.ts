
export interface Invoice {
    id: bigint;
    invoiceDate: Date;
    invoiceAmount: number;
    createdBy: string;
    invoiceTo: string;
    status: boolean;
    approvedBy: string;
    remark: string;
  }