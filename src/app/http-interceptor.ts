import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpEvent,
  HttpResponse,
  HttpRequest,
  HttpHandler,
  HttpErrorResponse,
} from "@angular/common/http";
import { Observable,  throwError  } from "rxjs";
import { catchError } from 'rxjs/operators';
import { throwToolbarMixedModesError } from '@angular/material/toolbar';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class AmoHttpInterceptor implements HttpInterceptor {

constructor(private router: Router) {}  

intercept(
    httpRequest: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(httpRequest).pipe(
        catchError((error:HttpErrorResponse) =>{
            
if (error instanceof HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
        console.error("Error Event");
    } else {
        console.log(`error status : ${error.status} ${error.statusText}`);
        switch (error.status) {
            case 401:      //login
                this.router.navigateByUrl("/login");
                break;
            case 403:     //forbidden
                this.router.navigateByUrl("/unauthorized");
                break;
        }
    } 
} else {
    console.error("some thing else happened");
}
return throwError(error);
        })

    )
  }



}
