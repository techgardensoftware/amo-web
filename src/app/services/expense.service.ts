import { Injectable, Inject } from "@angular/core";
import { Expense } from "../datamodel/Expense";
import { HttpClient, HttpHeaders} from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";

@Injectable()
export class ExpenseService {
  private expense: Expense[] = [];
  private baseUrl: string;
  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(private http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  getExpenses(): Observable<Expense[]> {
    return this.http.get<Expense[]>(environment.API_URL + "expense");
  }

  saveExpense(expense: Expense): any {
   console.log(expense);
    this.http.post(environment.API_URL + "expense", JSON.stringify(expense),
    {headers: this.headers}).subscribe(
      (result) => {
        return result;
      },
      (error) => console.error(error)
    );
  }
}
