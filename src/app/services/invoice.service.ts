import { Injectable, Inject } from "@angular/core";
import { Invoice } from "../datamodel/Invoice";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";


@Injectable()
export class InvoiceService {
  private expense: Invoice[] = [];
  private baseUrl: string;
  constructor(private http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  getInvoices(): Observable<Invoice[]> {
   
    return this.http.get<Invoice[]>(environment.API_URL + "invoice");
  }

  saveInvoice(invoice: Invoice): any {
    this.http.post(this.baseUrl + "api/invoice", invoice).subscribe(
      (result) => {
        return result;
      },
      (error) => console.error(error)
    );
  }
}
