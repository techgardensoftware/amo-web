import { Injectable, Inject } from "@angular/core";
import { Transport } from "../datamodel/Transport";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";

@Injectable()
export class TransportService {
  private expense: Transport[] = [];
  private baseUrl: string;
  constructor(private http: HttpClient) {}

  getTransport(): Observable<Transport[]> {
    return this.http.get<Transport[]>(environment.API_URL + "transport");
  }

  saveTransport(transport: Transport): any {
    this.http.post(environment.API_URL + "transport", transport).subscribe(
      (result) => {
        return result;
      },
      (error) => console.error(error)
    );
  }
}
