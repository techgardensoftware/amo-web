import { Injectable, Inject } from "@angular/core";
import { Vehicle } from "../datamodel/Vehicle";
import { HttpClient, HttpHeaders} from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";

@Injectable()
export class VehicleService {
  private vehicle: Vehicle[] = [];
  private baseUrl: string;
  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(private http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  getVehicles(): Observable<Vehicle[]> {
    return this.http.get<Vehicle[]>(environment.API_URL + "vehicle");
  }

  saveVehicle(vehicle: Vehicle): any {
   console.log(vehicle);
    this.http.post(environment.API_URL + "vehicle", JSON.stringify(vehicle),
    {headers: this.headers}).subscribe(
      (result) => {
        return result;
      },
      (error) => console.error(error)
    );
  }
}
